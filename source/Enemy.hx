package ;

import flixel.FlxSprite;
import FSM;
import Player;
import TiledLevel;
import flixel.util.FlxPath;
import flixel.util.FlxPoint;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.util.FlxAngle;

class Enemy extends FlxSprite {

	public var playState:PlayState;
	public var hasExploded:Bool = false;

	public var brain:FSM;

	public var TNTPlaceCountdown:Int = 0;

	//Path
	private var path:FlxPath;


	public var target:Player;
	
	public function new (x,y, ps:PlayState){
		super(x,y);

		loadGraphic("assets/images/Enemy.png", true, 32,32);

		setFacingFlip(FlxObject.RIGHT, false, false);
		setFacingFlip(FlxObject.LEFT, true, false);

		animation.add("Idle", [0,1,2], 8, false);
		animation.add("Down", [8,9,10,11,12,13,14], 8, false);
		animation.add("LR", [16,17,18,19], 8, false);
		animation.add("Up", [24,25,26,27,28,29,30], 8, false);

		playState = ps;

		brain = new FSM(AI_Attack);

		path = new FlxPath();



	}

	override public function update(){
		super.update();

		brain.update();

		if (TNTPlaceCountdown > 0 ){
			TNTPlaceCountdown -= 1;
		}

		if (velocity.x == 0 && velocity.y == 0){
			animation.play("Idle");
		}else{

			if (velocity.x > velocity.y && velocity.x > -velocity.y && velocity.x > -velocity.x){
				//Walk right
				animation.play("LR");
				facing = flixel.FlxObject.RIGHT;

			} else if (velocity.x < velocity.y && velocity.x < -velocity.y && velocity.x < -velocity.x) {
				//Walk left
				animation.play("LR");
				facing = flixel.FlxObject.LEFT;

			} else if ( velocity.y > velocity.x && velocity.y > -velocity.x && velocity.y > -velocity.y){
				//walk up
				animation.play("Down");
			}else{
				//Walk down
				animation.play("Up");
			}

		}
	}

	public function AI_Idle () {

	}

	public function AI_Attack () {

		if (playState == null || playState.mainPlayer == null || playState.enemies == null
		|| playState.clones ==null) return; //Stop crashing during state switches.


		var distanceToClone:Float = 1000000; //Used so we can keep track of smallest distance to clone in loop
		var closestClone:Player = null;

		for (clone in playState.clones){
			if (clone.alive == false) continue;
			for (map in playState.level.collidableTileLayers){
				if (map.ray(this.getMidpoint(),clone.getMidpoint())){
					if (this.getMidpoint().distanceTo(clone.getMidpoint()) < distanceToClone){
						distanceToClone =  this.getMidpoint().distanceTo(clone.getMidpoint());
						if (distanceToClone < 170){
							closestClone = clone;
						}

						if (TNTPlaceCountdown < 1){
							TNTPlaceCountdown = 100;

							var tnt:TNT = new TNT(this.x,this.y,playState);
							FlxAngle.rotatePoint(150, 0, 0, 0, flixel.util.FlxAngle.angleBetween(this,clone,true),tnt.velocity);

							playState.TNTs.add(tnt);
						}
					}
				}
			}
		}

		if (closestClone != null){
			//Don't move towards object if we are to close, as we will hit ourselves with tnt!
			
			if (this.getMidpoint().distanceTo(closestClone.getMidpoint()) > 40){
				flixel.util.FlxVelocity.moveTowardsObject(this,closestClone);
			}else{
				//we are close, stop
				this.velocity.set(0,0);
			}
			
		}else{
			this.velocity.set(0,0);
		}
		
	}

	public function AIPathfind () {

		if (path.nodes != null && path.nodes.length != 0) {

			//The enemy is chasing where it thinks the player is. Check if the player is still there...trace
			
			if (path.tail().distanceTo(target.getMidpoint()) > 30){
				trace("Bot moved, targeting new position");
				pathFindTo(target);
			}
			return;
		}

		if (path.finished == true || target == null) {
			for (clone in playState.clones){
				if (clone.alive && clone.hasExploded==false){
					if (clone.getMidpoint().distanceTo(this.getMidpoint()) < 70){
						
						
						trace("Started following bot");
						target = clone;


						pathFindTo(clone);
						return; //Break out of for loop so we don't try and follow them all.
					
					}

				}
			}
		}
	}

	public function pathFindTo (clone:Player){
		trace("FINDING PATH");

		// Find path to goal from unit to goal
		var pathPoints:Array<FlxPoint> = playState.level.collidableTileLayers[0].findPath(this.getMidpoint(), clone.getMidpoint());
		
		if (pathPoints != null) 
		{
			path.start(this, pathPoints);

		}
	}

	public function explode (){
		this.color = flixel.util.FlxColor.AZURE;

		hasExploded = true;
			
		playState.add(new Explosion(this.x,this.y));

		haxe.Timer.delay(function () {explodeNearby(40);},300);
		kill();
	}

	public function explodeNearby(radius:Float){
		if (playState == null || playState.mainPlayer == null || playState.enemies == null
			|| playState.clones ==null) return; //Stop crashing during state switches.

		if (playState.mainPlayer.getMidpoint().distanceTo(this.getMidpoint()) < radius){
			if (playState.mainPlayer.alive && playState.mainPlayer.hasExploded == false){
				playState.restartLevel();
				return;
			}
		}
		for (clone in playState.clones){
			if (clone.alive && clone.hasExploded==false){
				if (clone.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					clone.explode();
				}
			}
		}
		for (tnt in playState.TNTs){
			if (tnt.alive && tnt.hasExploded==false){
				if (tnt.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					tnt.explode();
				}
			}
		}
		for (enemy in playState.enemies){
			if (enemy.alive && enemy.hasExploded==false){
				if (enemy.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					enemy.explode();
				}
			}
		}
	}

}