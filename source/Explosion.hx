package ;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.system.FlxSound;

class Explosion extends FlxSprite {

	private var sound:FlxSound;

	public function new (x,y){
		super(x,y);

		sound = FlxG.sound.load("assets/sounds/Explosion.wav");
		sound.play();

		loadGraphic("assets/images/Explosion.png", true, 32,32);

		animation.add("pop", [0,1,2,3,4,5,6], 8, false);
		animation.play("pop");
		scale.set(3,3);
		FlxG.camera.shake(0.01, 0.05);
		haxe.Timer.delay(function () { kill(); },1000);
	}
}