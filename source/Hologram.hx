package ;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.system.FlxSound;
import PlayState;

class Hologram extends FlxSprite {

	private var sound:FlxSound;
	private var ps:PlayState;

	public function new (x,y,_ps){
		super(x,y);

		sound = FlxG.sound.load("assets/sounds/Materialise.wav");
		
		ps=_ps;

		loadGraphic("assets/images/Hologram.png", true, 16,16);

		animation.add("flash", [0,1], 8, true);
		animation.play("flash");
		haxe.Timer.delay(materialise,700);
	}

	public function materialise (){
		if (ps.clones == null) return; //Stop crashing during state switches.
		sound.play();
		ps.clones.add(new Player(x,y,true,ps));
		kill();
	}
}