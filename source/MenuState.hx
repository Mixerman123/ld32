package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.util.FlxDestroyUtil;

using flixel.util.FlxSpriteUtil;

/**
 * A FlxState which can be used for the game's menu.
 */



class MenuState extends FlxState
{

	private var _btnPlay:FlxButton;
	private var _lblTitle:FlxText;

	private var background:FlxSprite;



	/**
	 * Function that is called up when to state is created to set it up. 
	 */
	override public function create():Void
	{
		FlxG.scaleMode = new flixel.system.scaleModes.RatioScaleMode();

		background = new FlxSprite(0,0);
		background.loadGraphic("assets/images/Menu.png",false);
		flixel.util.FlxSpriteUtil.screenCenter(background);
		add(background);


		_btnPlay = new FlxButton(0, 0, "Play", clickPlay);
		//_btnPlay.screenCentre();
		flixel.util.FlxSpriteUtil.screenCenter(_btnPlay);
		add(_btnPlay);

		_lblTitle = new FlxText();
		flixel.util.FlxSpriteUtil.screenCenter(_lblTitle);
		_lblTitle.y-=90;
		_lblTitle.text = "Crash Bomb";
		_lblTitle.antialiasing=false;
		_lblTitle.setFormat( "assets/Fonts/PressStart2P.ttf", 16,flixel.util.FlxColor.BLACK,"center");

		
		_lblTitle.x=_btnPlay.x+10;
		


		add(_lblTitle);


		super.create();

	}
	
	/**
	 * Function that is called when this state is destroyed - you might want to 
	 * consider setting all objects this state uses to null to help garbage collection.
	 */
	override public function destroy():Void
	{
		_btnPlay = FlxDestroyUtil.destroy(_btnPlay);
		super.destroy();
	}


	/**
	 * Function that is called once every frame.
	 */
	override public function update():Void
	{
		super.update();



	}

	private function clickPlay():Void
	{
    	FlxG.switchState(new PlayState());
	}



}