package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxMath;
import flixel.group.FlxGroup;
import flixel.group.FlxTypedGroup;
import Hologram;
import TNT;
import Player;
import Enemy;
import flixel.util.FlxColor;
import flixel.util.FlxAngle;

class PlayState extends FlxState
{
	public var mainPlayer:Player;
	public var clones:FlxTypedGroup<Player>;

	public var enemies:FlxTypedGroup<Enemy>;
	public var TNTs:FlxTypedGroup<TNT>;

	public var level:TiledLevel;

	public var currentLevel:Int =0;

	override public function create():Void
	{
		super.create();

		mainPlayer = new Player(25,25,false,this);

		FlxG.camera.follow(mainPlayer);

		clones = new FlxTypedGroup<Player>();

		enemies = new FlxTypedGroup<Enemy>();
		TNTs = new FlxTypedGroup<TNT>();

		clones.add(mainPlayer);
		

		// Load the level's tilemaps
		level = new TiledLevel("assets/tiled/level"+currentLevel+".tmx");
		level.loadObjects(this);
		// Add stuff
		add(level.backgroundTiles);
		add(level.foregroundTiles);

		//add(mainPlayer);
		add(enemies);
		add(clones);
		add(TNTs);
		

	}
	
	
	override public function destroy():Void
	{
		super.destroy();

		close();
	}

	public function killAll(){
		clones.forEachAlive(function (p:Player){p.destroy();});
		enemies.forEachAlive(function (p:Enemy){p.destroy();});
		TNTs.forEachAlive(function (p:TNT){p.destroy();});
	}

	public function close (){
		killAll();

		clones = null;
		enemies=null;
		TNTs = null;
	}
	public function open(){
		clones = new FlxTypedGroup<Player>();

		enemies = new FlxTypedGroup<Enemy>();
		TNTs = new FlxTypedGroup<TNT>();

		

	}

	public function restartLevel (){
		FlxG.camera.fade(FlxColor.BLACK, 1, true);
		mainPlayer = new Player(25,25,false,this);

		killAll();
		close();

		

		open();
		FlxG.camera.follow(mainPlayer);
		clones.add(mainPlayer);



		//level = new TiledLevel("assets/tiled/"+currentLevel+"level.tmx");
		level.loadObjects(this);


		// Add stuff
		add(level.backgroundTiles);
		add(level.foregroundTiles);

		add(enemies);
		add(clones);
		add(TNTs);
	}

	public function gotoLevel (){
		FlxG.camera.fade(FlxColor.GREEN, 1, true);
		mainPlayer = new Player(25,25,false,this);

		killAll();
		close();

		

		open();
		FlxG.camera.follow(mainPlayer);
		clones.add(mainPlayer);


		currentLevel++;
		level = new TiledLevel("assets/tiled/level"+currentLevel+".tmx");
		level.loadObjects(this);


		// Add stuff
		add(level.backgroundTiles);
		add(level.foregroundTiles);

		add(enemies);
		add(clones);
		add(TNTs);
	}
	
	override public function update():Void
	{
		super.update();


		if (enemies.countLiving() == 0 ){
			//All enemies dead.
			gotoLevel();


		}


		level.collideWithLevel(mainPlayer);


		if (level.collidableTileLayers != null)
		{
			for (map in level.collidableTileLayers)
			{
				FlxG.collide(map, enemies);
				FlxG.collide(map, clones);
			}
		}
		FlxG.overlap(clones,clones,cloneHitClone);
		FlxG.overlap(clones,TNTs,cloneHitTnt);
		FlxG.overlap(enemies,clones,enemyHitClone);


		 if (FlxG.mouse.justPressedRight){
		 	var clone:Player = new Player(mainPlayer.x,mainPlayer.y,true,this);
			FlxAngle.rotatePoint(150, 0, 0, 0, flixel.util.FlxAngle.angleBetweenPoint(mainPlayer,FlxG.mouse.getWorldPosition(),true),clone.velocity);
			clone.drag.set(0,0);
			clone.elasticity = 1;
			clones.add(clone);
		 }

		if (FlxG.keys.anyJustPressed(["SPACE"])){
			add(new Hologram(mainPlayer.x,mainPlayer.y+10,this));
		}

		if(FlxG.mouse.justPressed){
			for (map in level.collidableTileLayers){
				for (clone in clones){
					if (map.ray(mainPlayer.getMidpoint(),clone.getMidpoint())){
						if (clone.getMidpoint().distanceTo(FlxG.mouse.getWorldPosition()) < 20){
							if (clone.alive){
								clone.explode();
							}
						}
					}
				}
			}
		}
	}

	function cloneHitClone (c:Player, cc:Player){
		if (c.isClone == false || cc.isClone == false) return;
		c.explode();
		cc.explode();
	}
	function cloneHitTnt (c:Player, t:TNT){
		t.explode();
	}
	function enemyHitClone (e:Enemy, t:Player){
		t.explode();
		e.explode();
	}

}