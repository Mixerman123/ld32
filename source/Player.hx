package ;


import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.util.FlxAngle;

import PlayState;


class Player extends FlxSprite {
	
	public var isClone:Bool = false;

	public var playState:PlayState;

	public var hasExploded:Bool = false;

	public function new (x:Float, y:Float, IsClone:Bool = false, ps:PlayState){
		super(x,y);
		loadGraphic("assets/images/Player.png", true, 16,16);

		setFacingFlip(FlxObject.RIGHT, false, false);
		setFacingFlip(FlxObject.LEFT, true, false);

		if (isClone==false) drag.x = drag.y = 1600;
		if (isClone) drag.set(0,0);

		animation.add("Idle", [0, 1], 8, false);
		animation.add("Down", [ 16,17,18,19], 8, false);
		animation.add("LR", [8,9,10,11], 8, false);
		animation.add("Up", [24,25,26,27], 8, false);

		//scale.set(2,2);

		playState = ps;


		isClone = IsClone;

		if (isClone){
			this.color = flixel.util.FlxColor.ROYAL_BLUE;
		}

	}

	override public function update(){
		super.update();

		if (isClone == false) movment();

			if (isClone){
				if (velocity.x == 0 && velocity.y == 0){
				animation.play("Idle");
			}else{

				if (velocity.x > velocity.y && velocity.x > -velocity.y && velocity.x > -velocity.x){
					//Walk right
					animation.play("LR");
					facing = flixel.FlxObject.RIGHT;

				} else if (velocity.x < velocity.y && velocity.x < -velocity.y && velocity.x < -velocity.x) {
					//Walk left
					animation.play("LR");
					facing = flixel.FlxObject.LEFT;

				} else if ( velocity.y > velocity.x && velocity.y > -velocity.x && velocity.y > -velocity.y){
					//walk up
					animation.play("Down");
				}else{
					//Walk down
					animation.play("Up");
				}

			}

			return;
		}

		if (velocity.x == 0 && velocity.y == 0){
			animation.play("Idle");
		}else{
			switch(facing)
			{
				case FlxObject.LEFT, FlxObject.RIGHT:
					animation.play("LR");
					
				case FlxObject.UP:
					animation.play("Up");
					
				case FlxObject.DOWN:
					animation.play("Down");
				default:
					
			}
		}

		
	}

	public function movment (){


		var _up:Bool = false;
		var _down:Bool = false;
		var _left:Bool = false;
		var _right:Bool = false;
		
		_up = FlxG.keys.anyPressed(["UP", "W"]);
		_down = FlxG.keys.anyPressed(["DOWN", "S"]);
		_left = FlxG.keys.anyPressed(["LEFT", "A"]);
		_right = FlxG.keys.anyPressed(["RIGHT", "D"]);
		
		if (_up && _down)
			_up = _down = false;
		if (_left && _right)
			_left = _right = false;
		
		if ( _up || _down || _left || _right)
		{

			//mA stores the angle we are traveling in.
			//facing stores the direction we are traveling in so we know
			//what sprite to display
			
			var mA:Float = 0;
			if (_up)
			{
				mA = -90;
				if (_left)
					mA -= 45;
				else if (_right)
					mA += 45;
					
				facing = FlxObject.UP;
			}
			else if (_down)
			{
				mA = 90;
				if (_left)
					mA += 45;
				else if (_right)
					mA -= 45;
				
				facing = FlxObject.DOWN;
			}
			else if (_left)
			{
				mA = 180;
				facing = FlxObject.LEFT;
			}
			else if (_right)
			{
				mA = 0;
				facing = FlxObject.RIGHT;
			}

			if (isClone != true) FlxAngle.rotatePoint(160, 0, 0, 0, mA, velocity);
			

		}
	}

	public function explode (){
		if (isClone){
			hasExploded = true;
			playState.add(new Explosion(this.x,this.y));
			haxe.Timer.delay(function () {explodeNearby(40);},300);
			kill();
		}else{
			playState.restartLevel();
		}
		
	}

	public function explodeNearby(radius:Float){

		if (playState == null || playState.mainPlayer == null || playState.enemies == null
			|| playState.clones ==null) return; //Stop crashing during state switches.

		if (playState.mainPlayer.getMidpoint().distanceTo(this.getMidpoint()) < radius){
			if (playState.mainPlayer.alive && playState.mainPlayer.hasExploded == false){
				playState.restartLevel();
				return;
			}
		}

		for (clone in playState.clones){
			if (clone.alive && clone.hasExploded==false){
				if (clone.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					clone.explode();
				}
			}
		}
		for (enemy in playState.enemies){
			if (enemy.alive && enemy.hasExploded==false){
				if (enemy.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					enemy.explode();
				}
			}
		}
		for (tnt in playState.TNTs){
			if (tnt.alive && tnt.hasExploded==false){
				if (tnt.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					tnt.explode();
				}
			}
		}
		
	}
}