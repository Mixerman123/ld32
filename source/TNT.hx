package ;

import flixel.FlxSprite;
import flixel.FlxG;
import flixel.system.FlxSound;
import PlayState;

class TNT extends FlxSprite {

	private var sound:FlxSound;
	private var playState:PlayState;
	public var hasExploded:Bool = false;
	public var usesTimer:Bool = true;

	public function new (x,y,_ps,useTimer = true){
		super(x,y);

		
		playState=_ps;

		this.drag.set(70,70);

		loadGraphic("assets/images/TNT.png", true, 16,16);

		animation.add("flash", [0,1], 8, true);
		animation.play("flash");

		usesTimer = useTimer;
	}

	public function explode (){
		
		hasExploded = true;
		playState.add(new Explosion(this.x,this.y));
		haxe.Timer.delay(function () {explodeNearby(40);},300);
		kill();
		
		
	}

	override public function update (){
		super.update();
		if (this.velocity.x == 0 && this.velocity.y ==0){
			if (usesTimer) explode();
		}
	}

	public function explodeNearby(radius:Float){

		if (playState == null || playState.mainPlayer == null || playState.enemies == null
			|| playState.clones ==null) return; //Stop crashing during state switches.

		if (playState.mainPlayer.getMidpoint().distanceTo(this.getMidpoint()) < radius){
			if (playState.mainPlayer.alive && playState.mainPlayer.hasExploded == false){
				playState.restartLevel();
				return;
			}
		}

		for (clone in playState.clones){
			if (clone.alive && clone.hasExploded==false){
				if (clone.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					clone.explode();
				}
			}
		}
		for (enemy in playState.enemies){
			if (enemy.alive && enemy.hasExploded==false){
				if (enemy.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					enemy.explode();
				}
			}
		}
		for (tnt in playState.TNTs){
			if (tnt.alive && tnt.hasExploded==false){
				if (tnt.getMidpoint().distanceTo(this.getMidpoint()) < radius){
					tnt.explode();
				}
			}
		}
		
	}
}